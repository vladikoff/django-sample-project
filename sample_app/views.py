from django.template import Context, loader
from django.http import HttpResponse

def index(request):

    t = loader.get_template('index.html')
    c = Context({
        "data_list" : [
            "item one",
            "item two",
            "item three",
        ],
        "item_list" : []
    })
    return HttpResponse(t.render(c))